import React, { useContext, useEffect } from "react";
import { Navbar } from "../../components/common/Navbar";
import { AppContext } from "../../lib/context";

export const HomePage: React.FC = () => {
  const { auth } = useContext(AppContext)!;

  return (
    <>
      <Navbar authed={auth ? true : false} className="bg-[#1f222c]" />
    </>
  );
};
