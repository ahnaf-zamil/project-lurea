import React, { useContext, useState } from "react";
import { Navigate } from "react-router-dom";
import { Navbar } from "../../components/common/Navbar";
import { RegisterForm } from "../../components/forms/RegisterForm";
import { AppContext } from "../../lib/context";

export const RegisterPage: React.FC = () => {
  const [formError, setFormError] = useState<string>("");
  const { auth } = useContext(AppContext)!;

  if (auth) {
    return <Navigate to="/" />;
  }

  console.log(auth);

  return (
    <>
      <div className="auth-bg fixed top-0 left-0 w-full h-full bg-red-200 -z-10"></div>
      <Navbar authed={auth} />
      <div className="w-full 2xl:w-7/12 fullscreen flex flex-col items-start justify-center px-8 md:px-32">
        {formError && (
          <div className="bg-red-500 mb-12 px-12 py-4 text-xl w-full text-center heading">
            <span className="font-semibold">Error:</span> {formError}
          </div>
        )}
        <div className="mb-12">
          <p className="gray font-semibold text-xl mb-4 ml-1">START FOR FREE</p>
          <h1 className="text-5xl font-semibold heading mb-8">
            Create new account<span className="blue">.</span>
          </h1>
          <h2 className="gray font-regular text-xl ml-1">
            Already a member?{" "}
            <a href="/auth/login" className="blue">
              Log In.
            </a>
          </h2>
        </div>
        <RegisterForm setFormError={setFormError} />
      </div>
      <p className="fixed bottom-4 right-4 text-sm">
        Photo by Tim Swaan on Unsplash
      </p>
    </>
  );
};
