import React, { useContext, useState } from "react";
import { Navbar } from "../../components/common/Navbar";
import { PublishPostForm } from "../../components/forms/PublishPostForm";
import { Input } from "../../components/ui/Input";
import { AppContext } from "../../lib/context";
import { AuthRequired } from "../../wrappers/AuthRequired";

export const CreatePostPage: React.FC = () => {
  const [formError, setFormError] = useState<string>("");
  const { auth } = useContext(AppContext)!;

  return (
    <AuthRequired>
      <Navbar authed={auth ? true : false} className="bg-[#1f222c]" />
      <div className="py-16 px-6 container mx-auto xl:px-52">
        <div className="heading">
          <h1 className="mb-3 font-semibold text-xl">Publish a post</h1>
          <p className="text-gray-300 mb-3">
            Create a new post on the platform and show your creations!
          </p>
        </div>
        <hr className="border border-gray-600" />
        <PublishPostForm setFormError={setFormError} />
      </div>
    </AuthRequired>
  );
};
