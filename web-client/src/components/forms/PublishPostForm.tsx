import React, { createRef, useEffect, useState } from "react";
import { WithContext as ReactTags, Tag } from "react-tag-input";
import { Input } from "../ui/Input";

interface ImagesInputProps {
  images: Array<string>;
  setImages: React.Dispatch<React.SetStateAction<Array<string>>>;
}

const ImagesInput: React.FC<ImagesInputProps> = ({ images, setImages }) => {
  const inpRef = createRef<HTMLInputElement>();
  const [disabledInp, setDisabledInp] = useState<boolean>(false);

  const handleImgSelect = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();

    const file = e.target.files![0];
    if (!file) {
      return;
    }
    if (file.size / (1024 * 1024) > 5) {
      return alert("Image size cannot be greater than 5 MB");
    }

    const reader = new FileReader();
    reader.onloadend = () => {
      setImages([...images, reader.result as string]);
    };
    reader.readAsDataURL(file);
  };

  useEffect(() => {
    setDisabledInp(images.length === 5);
  }, [images]);

  return (
    <div>
      <h3 className="text-lg heading mb-[4px]">Images</h3>
      <h4 className="text-gray-400 pb-2">
        Add images for your post. Max 5 images.
      </h4>
      <input
        ref={inpRef}
        className="hidden"
        type="file"
        accept="image/png, image/jpeg"
        onChange={handleImgSelect}
      />
      <div className="mt-[20px] flex gap-8 flex-wrap">
        {images.map((val, i) => (
          <div className="relative">
            <div
              onClick={() => {
                const arr = [...images];
                arr.splice(i, 1);
                setImages(arr);
              }}
              className="h-5 w-5 bg-gray-900 hover:bg-gray-300 hover:text-black cursor-pointer absolute -top-2 -right-2 rounded-full"
            ></div>
            <img className="h-48" src={val} />
          </div>
        ))}
        <div
          onClick={() => {
            inpRef.current?.click();
          }}
          className={`${
            disabledInp ? "hidden" : ""
          } cursor-pointer border border-dashed border-gray-300 w-56 h-48 flex flex-col items-center justify-center text-gray-300 font-semibold gap-4`}
        >
          <p className="text-3xl">
            <i className="fa-solid fa-camera"></i>
          </p>
          <p className="text-xl heading">Add Image</p>
          <small>Max: 5 mb</small>
        </div>
      </div>
    </div>
  );
};

interface TagsInputProps {
  tags: Array<Tag>;
  setTags: React.Dispatch<React.SetStateAction<Array<Tag>>>;
}

const TagsInput: React.FC<TagsInputProps> = ({ tags, setTags }) => {
  const delimiters = [188, 13, 32, 9]; // Comma, enter, space, tab

  return (
    <div>
      <h3 className="text-lg heading mb-[4px]">Tags</h3>
      <h4 className="text-gray-400 pb-2">
        Add tags to categorise your post. Max 5 tags can be used
      </h4>
      <ReactTags
        tags={tags}
        delimiters={delimiters}
        handleDelete={(i) => setTags(tags.filter((tag, index) => index !== i))}
        handleAddition={(tag) => {
          if (tags.length < 5) {
            setTags([...tags, tag]);
          }
        }}
        handleDrag={(tag, currPos, newPos) => {
          const newTags = tags.slice();
          newTags.splice(currPos, 1);
          newTags.splice(newPos, 0, tag);
          setTags(newTags);
        }}
        inputFieldPosition="bottom"
        classNames={{
          tags: "border border-gray-500 py-2 hover:border-white rounded flex mt-3 flex-wrap",
          selected: "flex mx-3 gap-2",
          tag: "bg-[#171921] px-4 flex py-2 rounded gap-2",
          tagInputField: `${
            tags.length === 5 ? "hidden" : ""
          } h-full bg-transparent outline-none w-full`,
          tagInput: "w-full",
        }}
        maxLength={5}
        placeholder="Add Tag"
        // autocomplete
      />
    </div>
  );
};

interface PublishPostFormProps {
  setFormError: React.Dispatch<React.SetStateAction<string>>;
}

export const PublishPostForm: React.FC<PublishPostFormProps> = () => {
  const [images, setImages] = useState<Array<string>>([]);
  const [tags, setTags] = useState<Array<Tag>>([]);

  return (
    <form className="mt-10 flex flex-col gap-8">
      <Input
        description="Set the title of your post. Max 50 characters."
        label="Title"
        name="title"
        type="text"
      />
      <Input
        description="Set the description of your post. Max 1000 characters."
        label="Description"
        name="description"
        type="text"
        textarea
      />
      <ImagesInput images={images} setImages={setImages} />
      <TagsInput tags={tags} setTags={setTags} />
    </form>
  );
};
