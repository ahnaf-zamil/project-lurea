import React, { SyntheticEvent, useEffect, useState } from "react";
import { useDebouncedValue } from "../../hooks/debounce";
import { httpClient } from "../../lib/api";
import { ENDPOINTS } from "../../lib/api/endpoints";
import { AuthInput } from "../ui/AuthInput";
import { Button } from "../ui/Button";
import { AxiosError, isAxiosError } from "axios";

interface Props {
  setFormError: React.Dispatch<React.SetStateAction<string>>;
}

export const RegisterForm: React.FC<Props> = ({ setFormError }) => {
  const [fullName, setFullName] = useState<string>();
  const [username, setUsername] = useState<string>("");
  const [email, setEmail] = useState<string>();
  const [password, setPassword] = useState<string>();

  const debouncedUsername = useDebouncedValue(username, 500);
  const [usernameErr, setUsernameErr] = useState<string>("");

  useEffect(() => {
    if (debouncedUsername.length >= 4) {
      httpClient
        .post(ENDPOINTS.CHECK_USERNAME, { username: debouncedUsername })
        .then((resp) => {
          setUsernameErr("");
        })
        .catch((err: AxiosError) => {
          setUsernameErr((err.response?.data as any).message);
        });
    }
  }, [debouncedUsername]);

  const handleSubmit = async (e: SyntheticEvent) => {
    e.preventDefault();
    setFormError("");

    try {
      await httpClient.post(ENDPOINTS.REGISTER_USER, {
        fullname: fullName,
        username,
        email,
        password,
      });

      console.log("Successfully registered");
      window.location.href = "/";
    } catch (err: any) {
      if (isAxiosError(err)) {
        setFormError((err.response?.data as any).message);
      }
    }
  };

  return (
    <form className="w-full flex flex-col gap-6" onSubmit={handleSubmit}>
      <div className="flex flex-col lg:flex-row justify-between gap-6">
        <AuthInput
          onChange={(e) => setFullName(e.target.value)}
          name="fullname"
          label="Full Name"
          type="text"
          minLength={4}
          maxLength={30}
          value={fullName}
        />
        <AuthInput
          onChange={(e) => {
            setUsernameErr("");
            setUsername(e.target.value.toLowerCase().replace(/\s/g, ""));
          }}
          name="username"
          label="Username"
          type="text"
          minLength={4}
          maxLength={30}
          error={usernameErr}
          value={username}
        />
      </div>
      <AuthInput
        onChange={(e) => setEmail(e.target.value)}
        name="email"
        label="Email Address"
        type="email"
        value={email}
      />
      <AuthInput
        onChange={(e) => setPassword(e.target.value)}
        name="password"
        label="Password"
        type="password"
        minLength={8}
        value={password}
      />
      <Button
        className="mt-4 px-8 py-4 bg-blue-500 hover:bg-blue-400"
        text="Create Account"
      />
    </form>
  );
};
