import React, { SyntheticEvent, useState } from "react";
import { httpClient } from "../../lib/api";
import { ENDPOINTS } from "../../lib/api/endpoints";
import { AuthInput } from "../ui/AuthInput";
import { Button } from "../ui/Button";
import { isAxiosError } from "axios";

interface Props {
  setFormError: React.Dispatch<React.SetStateAction<string>>;
}

export const LoginForm: React.FC<Props> = ({ setFormError }) => {
  const [email, setEmail] = useState<string>();
  const [password, setPassword] = useState<string>();

  const handleSubmit = async (e: SyntheticEvent) => {
    e.preventDefault();
    setFormError("");

    try {
      await httpClient.post(ENDPOINTS.LOGIN_USER, {
        email,
        password,
      });

      console.log("Successfully logged in");
      window.location.href = "/";
    } catch (err: any) {
      if (isAxiosError(err)) {
        setFormError((err.response?.data as any).message);
      }
    }
  };

  return (
    <form className="w-full flex flex-col gap-6" onSubmit={handleSubmit}>
      <AuthInput
        onChange={(e) => setEmail(e.target.value)}
        name="email"
        label="Username or Email"
        type="text"
        value={email}
      />
      <AuthInput
        onChange={(e) => setPassword(e.target.value)}
        name="password"
        label="Password"
        type="password"
        minLength={8}
        value={password}
      />
      <Button
        className="mt-4 px-8 py-4 bg-blue-500 hover:bg-blue-400"
        text={"Log In"}
      />
    </form>
  );
};
