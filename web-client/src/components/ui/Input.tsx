import React, { useEffect, useState } from "react";

interface Props {
  name: string;
  label: string;
  description?: string;
  type: string;
  value?: string;
  minLength?: number;
  maxLength?: number;
  onChange?: (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => void;
  error?: string;
  textarea?: boolean;
}

export const Input: React.FC<Props> = ({
  name,
  label,
  type,
  minLength,
  maxLength,
  value,
  onChange,
  error,
  description,
  textarea,
}) => {
  const [focused, setFocused] = useState<boolean>(false);

  return (
    <span className="w-full">
      <h3 className="text-lg heading mb-[4px]">{label}</h3>
      {description && <h4 className="text-gray-400 pb-2">{description}</h4>}
      <div
        className={`${error ? "ring-2 ring-red-500" : ""} ${
          focused ? " border border-white" : ""
        } flex-1 mt-[12px] transition duration-200 w-full ring-offset-[#272a37] ring-offset-2 heading bg-transparent border border-gray-500 w-auto px-3 py-3 rounded flex flex-col gap-1`}
      >
        {textarea ? (
          <textarea
            onFocus={() => setFocused(true)}
            onBlur={() => setFocused(false)}
            name={name}
            minLength={minLength}
            maxLength={maxLength}
            onChange={(e) => {
              if (onChange) onChange(e);
            }}
            value={value}
            placeholder={label}
            className="outline-none bg-transparent"
            required
            rows={5}
          ></textarea>
        ) : (
          <input
            onFocus={() => setFocused(true)}
            onBlur={() => setFocused(false)}
            type={type}
            name={name}
            minLength={minLength}
            maxLength={maxLength}
            onChange={(e) => {
              if (onChange) onChange(e);
            }}
            value={value}
            placeholder={label}
            className="outline-none bg-transparent"
            required
          />
        )}
      </div>
      <p className="text-red-500 pt-3 px-5">{error}</p>
    </span>
  );
};
