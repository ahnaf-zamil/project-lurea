import React, { ReactNode } from "react";

interface Props {
  className?: string;
  text: string | ReactNode;
  link?: string;
}

export const Button: React.FC<Props> = ({ link, className, text }) => {
  const btn = (
    <button className={`rounded-full heading font-semibold ` + className}>
      {text}
    </button>
  );

  return <div>{link ? <a href={link}>{btn}</a> : btn}</div>;
};
