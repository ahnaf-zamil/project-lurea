import React, { useEffect, useState } from "react";

interface Props {
  name: string;
  label: string;
  type: string;
  value?: string;
  minLength?: number;
  maxLength?: number;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  error?: string;
}

export const AuthInput: React.FC<Props> = ({
  name,
  label,
  type,
  minLength,
  maxLength,
  value,
  onChange,
  error,
}) => {
  const [focused, setFocused] = useState<boolean>(false);

  return (
    <span className="w-full">
      <div
        className={`${
          focused || error ? "ring-2" : ""
        } flex-1 transition duration-200 w-full ${
          error ? "ring-red-500" : "ring-blue-500"
        } ring-offset-[#272a37] ring-offset-2 heading gray-bg w-auto px-8 py-3 rounded-3xl flex flex-col gap-1`}
      >
        <p className="gray text-sm">{label}</p>
        <input
          onFocus={() => setFocused(true)}
          onBlur={() => setFocused(false)}
          type={type}
          name={name}
          minLength={minLength}
          maxLength={maxLength}
          onChange={onChange}
          value={value}
          className="outline-none bg-transparent font-semibold text-xl"
          required
        />
      </div>
      <p className="text-red-500 pt-3 px-5">{error}</p>
    </span>
  );
};
