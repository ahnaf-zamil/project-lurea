import React from "react";
import { Button } from "../ui/Button";

interface Props {
  className?: string;
  authed?: boolean;
}

export const Navbar: React.FC<Props> = ({ className = "", authed }) => {
  return (
    <div
      className={
        "w-full flex justify-between items-center px-20 py-2 mx-auto " +
        className
      }
    >
      <div className="flex gap-12 items-center">
        <a href="/" className="font-bold text-2xl">
          ALPINO
        </a>
        <ul className="border-x border-gray-600 px-6 flex items-center gap-6 font-bold heading">
          <li className="hover:text-sky-400 transition duration-200">
            <a href="">Explore</a>
          </li>
          <li className="hover:text-sky-400 transition duration-200">
            <a href="">About</a>
          </li>
        </ul>
      </div>
      {authed && (
        <div className="gray-bg border border-gray-600 rounded-full w-4/12 px-4 flex items-center">
          <p className="gray pr-4">
            <i className="fa-solid fa-magnifying-glass"></i>
          </p>
          <input
            className="w-full bg-transparent h-full py-2 outline-none"
            placeholder="Search for projects, art, designs, robots..."
            type="text"
          />
        </div>
      )}
      <ul className="heading flex gap-8 font-semibold items-center">
        {authed ? (
          <>
            <li className="hover:text-sky-400">
              <p className="cursor-pointer text-xl">
                <i className="fa-solid fa-bell"></i>
              </p>
            </li>
            <li>
              <Button
                link="/post/create"
                className="bg-emerald-500 hover:bg-emerald-400 text-black rounded px-6 py-2"
                text={
                  <p className="flex gap-3 items-center heading font-black">
                    <i className="fa-solid fa-plus"></i>
                    <span>Publish</span>
                  </p>
                }
              />
            </li>
            <li>
              <p className="hover:bg-gray-200 cursor-pointer rounded text-black bg-white w-10 h-10 flex items-center justify-center text-xl">
                <i className="fa-solid fa-user"></i>
              </p>
            </li>
          </>
        ) : (
          <>
            <li>
              <a href="/auth/register">Join</a>
            </li>
            <li>
              <Button
                link="/auth/login"
                className="px-6 py-2 bg-blue-400 hover:bg-blue-500"
                text="Log In"
              />
            </li>
          </>
        )}
      </ul>
    </div>
  );
};
