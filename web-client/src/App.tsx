import { useState, useEffect } from "react";
import { httpClient } from "./lib/api";
import { ENDPOINTS } from "./lib/api/endpoints";
import { AppContext } from "./lib/context";
import { IAuthedUser } from "./lib/types";
import { AppRouter } from "./Router";

export const App: React.FC = () => {
  const [auth, setAuth] = useState<IAuthedUser | undefined>();
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    httpClient
      .get(ENDPOINTS.GET_CURRENT_USER)
      .then((resp) => setAuth(resp.data))
      .finally(() => setLoading(false));
  }, []);

  if (loading) {
    return <>Loading</>;
  }

  return (
    <AppContext.Provider
      value={{
        auth: auth,
      }}
    >
      <AppRouter />
    </AppContext.Provider>
  );
};
