import { ReactNode, useContext } from "react";
import { Navigate } from "react-router-dom";
import { AppContext } from "../lib/context";

interface Props {
  children: ReactNode;
}

export const AuthRequired: React.FC<Props> = ({ children }) => {
  const { auth } = useContext(AppContext)!;

  if (!auth) {
    return <Navigate to="/auth/login" />;
  }
  return <>{children}</>;
};
