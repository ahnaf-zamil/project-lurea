import { useEffect, useState } from "react";

/* Hook for debouncing a value change */
export const useDebouncedValue = <T>(input: T, time = 500) => {
  const [debouncedValue, setDebouncedValue] = useState(input);

  // Set timeout every time input is changed before setting it into state
  useEffect(() => {
    const timeout = setTimeout(() => {
      setDebouncedValue(input);
    }, time);

    return () => {
      clearTimeout(timeout);
    };
  }, [input]);

  return debouncedValue;
};
