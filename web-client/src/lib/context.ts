import { createContext } from "react";
import { IAuthedUser } from "./types";

interface AppContextInterface {
  auth?: IAuthedUser;
}

export const AppContext = createContext<AppContextInterface | null>(null);
