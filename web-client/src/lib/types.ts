export interface IUser {
  id: string;
  fullname: string;
  username: string;
  created_at: string;
}

export interface IAuthedUser extends IUser {
  email: string;
}
