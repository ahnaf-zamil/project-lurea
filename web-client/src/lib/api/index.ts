import axios from "axios";

const API_URL = "http://localhost:5000";

export const httpClient = axios.create({
  baseURL: API_URL,
  withCredentials: true,
});
