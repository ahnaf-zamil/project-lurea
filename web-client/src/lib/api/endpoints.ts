export const ENDPOINTS = {
  REGISTER_USER: "/auth/register",
  LOGIN_USER: "/auth/login",
  CHECK_USERNAME: "/auth/check/username",

  GET_CURRENT_USER: "/user/@me",
};
