import { BrowserRouter, Route, Routes } from "react-router-dom";
import { LoginPage } from "./pages/auth/LoginPage";
import { RegisterPage } from "./pages/auth/RegisterPage";
import { HomePage } from "./pages/main/Home";
import { CreatePostPage } from "./pages/post/CreatePost";

export const AppRouter: React.FC = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route index element={<HomePage />} />
        <Route path="auth">
          <Route path="register" element={<RegisterPage />} />
          <Route path="login" element={<LoginPage />} />
        </Route>
        <Route path="post">
          <Route path="create" element={<CreatePostPage />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};
