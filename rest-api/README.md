# REST API

This is the REST API for Alpino. Uses the following technologies:

- Framework: Flask (Python)
- Database: PostgreSQL
- Migration: Alembic (Flask-Migrate)
- Image CDN: MinIO
- Documentation: Swagger

## How to run

1. Clone the repository using
   ```commandline
   git clone https://github.com/Untitled-Sandbox-Project/rest-api
   ```
2. Rename `.env.example` to `.env` and configure the values properly
3. Install dependencies using
   ```commandline
   pip install -r requirements.txt
   ```
4. Start the server using

```commandline
python wsgi.py
```

## Documentation

Documentation for the API is at the `/docs` route. For local deployment, it would be at `https://localhost:5000/docs`.
