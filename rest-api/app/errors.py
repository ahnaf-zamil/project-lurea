from flask import jsonify
from werkzeug.exceptions import HTTPException


def handle_http_err(error: HTTPException):
    msg = error.description
    code = error.code
    return jsonify({"status": code, "message": msg}), code
