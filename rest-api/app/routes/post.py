from flask import Blueprint, current_app, jsonify, abort, request
from ..lib.auth import auth_required, get_authed_user
from ..lib.schemas import create_post_schema
from ..lib.validator import validate_body, clean_or_throw
from ..models import Post, User, PostImage, Tag
from ..lib.ext import db
from ..lib.misc import slugify
from ..lib.image import get_image_data, upload_post_img
from uuid import uuid4
from typing import Optional

router = Blueprint("post", import_name=__name__, url_prefix="/post")


@router.post("/create")
@auth_required
def create_post():
    """
    Create a new post
    ---
    tags:
        - post
    parameters:
        -   in: body
            name: body
            schema:
                id: CreatePostSchema
                required:
                    - title
                    - description
                    - images
                properties:
                    title:
                        type: string
                        description: Title of post
                        minLength: 5
                        maxLength: 50
                    description:
                        type: string
                        description: Description of post
                        minLength: 5
                        maxLength: 1000
                    tags:
                        type: array
                        items:
                            type: string
                            minLength: 2
                            maxLength: 10
                        minItems: 1
                        maxItems: 5
                    images:
                        type: array
                        items:
                            type: object
                            properties:
                                data:
                                    type: string
                                    description: Base64 encoded image
                        example:
                            -   name: data:image/png;base64,iVBORw0KGgoAA....
    responses:
        201:
            description: Successfully created post
        400:
            description: Invalid body values
        401:
            description: Not authenticated
    """
    user = get_authed_user()
    if not user:
        abort(401, "Not logged in")

    body_err = validate_body(create_post_schema)
    if body_err:
        current_app.logger.info(f"Validator failed: {body_err[0]}")
        abort(400, "Bad Input")

    # Adding post record
    post_id = uuid4().hex
    slug = slugify(request.json["title"], post_id)
    new_post = Post(
        id=post_id,
        title=request.json["title"],
        description=request.json["description"],
        slug=slug,
        author_id=user.id,
    )
    db.session.add(new_post)

    # Uploading images
    images = request.json["images"]
    for i, d in enumerate(images):
        img_data = get_image_data(d["data"])
        fname = upload_post_img(post_id, i, img_data)
        new_img = PostImage(filename=fname.split("/")[-1], post_id=post_id)
        db.session.add(new_img)

    # Adding tags to post
    tag_list = [i.lower() for i in request.json["tags"]]

    # Tags are filtered to only include the ones that have not been created in DB
    existing_tags = Tag.query.filter(Tag.value.in_(tag_list)).all()
    # Associating the existing tags with the post
    new_post.tags.extend(existing_tags)
    existing_tag_names = [i.value for i in existing_tags]

    # Create the new tags that were not present in DB before
    new_tags = [clean_or_throw(i) for i in tag_list if i not in existing_tag_names]

    for i in new_tags:
        new_tag = Tag(id=uuid4().hex, value=i.lower())
        # Associating the new tag with the post
        new_post.tags.append(new_tag)

    db.session.commit()

    return (
        jsonify(
            {
                "status": 201,
                "post_id": new_post.id,
                "url": f"/@{user.username}/{new_post.slug}",
            }
        ),
        201,
    )


@router.get("/<string:username>/<string:slug_or_id>")
def get_post(username: str, slug_or_id: str):
    """
    Get/View post
    ---
    tags:
        - post
    parameters:
        -   in: path
            name: username
            schema:
                type: string
            required: true
            description: Username of author
        -   in: path
            name: slug_or_id
            schema:
                type: string
            required: true
            description: Slug/ID of post
    """
    username = username.lower()

    existing_author: Optional[User] = User.query.filter_by(username=username).first()
    existing_post: Optional[Post] = Post.query.filter(
        (Post.id == slug_or_id) | (Post.slug == slug_or_id),
        Post.author_id == existing_author.id,
        Post.deleted == False,
    ).first()

    if not existing_post:
        abort(404, "Post not found")

    resp = existing_post.json(preview=False)
    resp.update({"images": [i.filename for i in existing_post.images]})
    resp.update({"author": existing_author.json()})

    return resp
