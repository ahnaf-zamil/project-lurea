from flask import Blueprint
from . import auth, user, post

all_routes = Blueprint("routes", import_name=__name__)

all_routes.register_blueprint(auth.router)
all_routes.register_blueprint(user.router)
all_routes.register_blueprint(post.router)
