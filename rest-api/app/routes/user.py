from flask import Blueprint, abort
from ..lib.auth import auth_required, get_authed_user

router = Blueprint("user", import_name=__name__, url_prefix="/user")


@router.get("/@me")
@auth_required
def get_logged_in_user():
    """
    Get the currently logged in user's profile
    ---
    tags:
        - user
    responses:
        200:
            description: Returns user successfully
        401:
            description: Not authenticated
    """
    user = get_authed_user()
    if not user:
        abort(401, "Not logged in")
    return user.json(preview=False)
