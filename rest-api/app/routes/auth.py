import jwt

from typing import Optional
from flask import Blueprint, current_app, request, jsonify, make_response, abort
from ..models import User
from ..lib.schemas import register_user_schema, login_user_schema, check_username_schema
from ..lib.validator import validate_body, clean_or_throw
from ..lib.ext import bcrypt, db
from ..config import AppConfig

router = Blueprint("auth", import_name=__name__, url_prefix="/auth")


@router.post("/register")
def register_user():
    """
    Create/Register a user
    ---
    tags:
        - auth
    parameters:
        -   in: body
            name: body
            schema:
                id: RegisterSchema
                required:
                    - username
                    - fullname
                    - email
                    - password
                properties:
                    username:
                        type: string
                        description: Username for user
                        minLength: 4
                        maxLength: 30
                    fullname:
                        type: string
                        description: Actual name of user
                        minLength: 4
                        maxLength: 30
                    email:
                        type: string
                        description: Email for user
                        format: email
                    password:
                        type: string
                        description: Password for user
                        minLength: 8
    responses:
        201:
            description: Successfully created user
        400:
            description: Invalid body values
        409:
            description: User already exists (with same username or email)
    """
    body_err = validate_body(register_user_schema)
    if body_err:
        current_app.logger.info(f"Validator failed: {body_err[0]}")
        abort(400, "Bad Input")

    username = clean_or_throw(request.json["username"]).lower()
    email = request.json["email"]
    fullname = clean_or_throw(request.json["fullname"])

    existing_user: Optional[User] = User.query.filter(
        (User.username == username) | (User.email == email), User.deleted == False
    ).first()
    if existing_user:
        abort(409, "User already exists")

    hashed_pw = bcrypt.generate_password_hash(request.json["password"])
    new_user = User(
        username=username,
        fullname=fullname,
        email=email,
        password=hashed_pw.decode("utf-8"),
    )
    db.session.add(new_user)
    db.session.commit()

    # Setting auth state (HTTPOnly cookie)
    auth_jwt = jwt.encode({"id": new_user.id}, AppConfig.JWT_KEY, algorithm="HS256")

    resp = make_response({"status": 201, "message": "User created"}, 201)
    resp.set_cookie("auth", value=auth_jwt, httponly=True, samesite="None", secure=True)
    return resp


@router.post("/login")
def login_user():
    """
    Authenticate/Login a user
    ---
    tags:
        - auth
    parameters:
        -   in: body
            name: body
            schema:
                id: LoginSchema
                required:
                    - email
                    - password
                properties:
                    email:
                        type: string
                        description: Email for user
                    password:
                        type: string
                        description: Password for user
    responses:
        200:
            description: Successfully logged in
        400:
            description: Invalid body values
        401:
            description: Invalid credentials (Username/email/password)
    """
    body_err = validate_body(login_user_schema)
    if body_err:
        current_app.logger.info(f"Validator failed: {body_err[0]}")
        abort(400, "Bad input")

    email = request.json["email"]
    password = request.json["password"]

    # Matching for both username and email to allow options for entering either one
    existing_user: Optional[User] = User.query.filter(
        (User.username == email) | (User.email == email), User.deleted == False
    ).first()
    if not existing_user:
        abort(401, "Invalid credentials")

    if not bcrypt.check_password_hash(existing_user.password, password):
        abort(401, "Invalid credentials")

    # Setting auth state (HTTPOnly cookie)
    auth_jwt = jwt.encode(
        {"id": existing_user.id}, AppConfig.JWT_KEY, algorithm="HS256"
    )

    resp = make_response({"status": 200})
    resp.set_cookie("auth", value=auth_jwt, httponly=True, samesite="None", secure=True)
    return resp


@router.post("/check/username")
def check_username():
    """
    Check if username already exists or not
    ---
    tags:
        - auth
    parameters:
        -   in: body
            name: body
            schema:
                required:
                    - username
                properties:
                    username:
                        type: string
                        description: Username for user
    responses:
        200:
            description: Successfully logged in
        409:
            description: A user with this username already exists
        400:
            description: Invalid body values
    """
    body_err = validate_body(check_username_schema)
    if body_err:
        current_app.logger.info(f"Validator failed: {body_err[0]}")
        abort(400, "Bad Input")

    username = clean_or_throw(request.json["username"]).lower()
    existing_user: Optional[User] = User.query.filter_by(username=username).first()
    if existing_user:
        abort(409, "Username taken")
    else:
        return jsonify({"status": 200})
