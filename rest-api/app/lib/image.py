import base64
import re

from PIL import Image as Im
from io import BytesIO
from .ext import minio_client
from ..config import AppConfig


def get_image_data(data: str):
    based64_data = re.sub("^data:image/.+;base64,", "", data)
    byte_data = base64.b64decode(based64_data)
    image_data = BytesIO(byte_data)
    return Im.open(image_data)


def upload_post_img(post_id: str, index: int, img: Im) -> str:
    found = minio_client.bucket_exists(AppConfig.MINIO_BUCKET)
    if not found:
        minio_client.make_bucket(AppConfig.MINIO_BUCKET)

    bio = BytesIO()
    img.save(bio, format="PNG", optimize=True, quality=80)
    bio.seek(0)

    fname = f"posts/{post_id}/{index}.{img.format}"
    minio_client.put_object(
        AppConfig.MINIO_BUCKET, fname, bio, length=bio.getbuffer().nbytes
    )

    return fname
