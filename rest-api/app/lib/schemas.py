# ----- Auth -----

register_user_schema = {
    "type": "object",
    "properties": {
        "username": {"type": "string", "minLength": 4, "maxLength": 30},
        "fullname": {"type": "string", "minLength": 4, "maxLength": 30},
        "email": {
            "type": "string",
            "pattern": r"([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+",
        },
        "password": {"type": "string", "minLength": 8},
    },
    "required": ["username", "fullname", "email", "password"],
}

login_user_schema = {
    "type": "object",
    "properties": {
        "email": {"type": "string", "minLength": 4},
    },
    "required": ["email", "password"],
}

check_username_schema = {
    "type": "object",
    "properties": {
        "username": {"type": "string", "minLength": 4, "maxLength": 30},
    },
    "required": ["username"],
}


# ----- Post -----

create_post_schema = {
    "type": "object",
    "properties": {
        "title": {"type": "string", "minLength": 5, "maxLength": 50},
        "description": {"type": "string", "minLength": 5, "maxLength": 1000},
        "tags": {
            "type": "array",
            "minContains": 1,
            "maxContains": 5,
            "contains": {"type": "string", "minLength": 2, "maxLength": 10},
        },
        "images": {
            "type": "array",
            "minContains": 1,
            "maxContains": 5,
            "contains": {
                "type": "object",
                "properties": {
                    "data": {"type": "string"},
                },
                "required": ["data"],
            },
        },
    },
    "required": ["title", "description", "images", "tags"],
}
