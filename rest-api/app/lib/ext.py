from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS
from flask_bcrypt import Bcrypt
from minio import Minio
from ..config import AppConfig


db = SQLAlchemy()
migrate = Migrate()
cors = CORS(supports_credentials=True)
bcrypt = Bcrypt()
minio_client = Minio(
    AppConfig.MINIO_ENDPOINT,
    access_key=AppConfig.MINIO_ACCESS_KEY,
    secret_key=AppConfig.MINIO_SECRET_KEY,
    secure=False,
)
