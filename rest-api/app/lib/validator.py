import collections
import re

from flask import request, abort
from flask_inputs import Inputs
from flask_inputs.validators import JsonSchema
from wtforms.fields import Field
from wtforms.form import BaseForm
from typing import Optional


class InputValidator(Inputs):
    def __init__(self, schema):
        self.json = [JsonSchema(schema=schema)]
        self.errors = []

        self._request = request
        self._forms = dict()

        for name in dir(self):
            if not name.startswith("_") and name not in [
                "errors",
                "validate",
                "valid_attrs",
            ]:
                input = getattr(self, name)
                fields = dict()

                if isinstance(input, dict):
                    for field, validators in input.items:
                        fields[field] = Field(validators=validators)
                elif isinstance(input, collections.abc.Iterable):
                    fields["_input"] = Field(validators=input)

                self._forms[name] = BaseForm(fields)


def validate_body(schema) -> Optional[list]:
    inputs = InputValidator(schema)
    if inputs.validate():
        return None
    else:
        return inputs.errors


def clean_or_throw(val: str):
    regex = re.compile(r"[@!#$%^&*()<>?/\|}{~:]")
    if not regex.search(val):
        return val
    else:
        abort(400, "Bad Input")
