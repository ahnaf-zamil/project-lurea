import re
import unicodedata

_slugify_strip_re = re.compile(r"[^\w\s-]")
_slugify_hyphenate_re = re.compile(r"[-\s]+")


def slugify(title: str, post_id: str):
    """
    Normalizes string, converts to lowercase, removes non-alpha characters,
    and converts spaces to hyphens.

    From Django's "django/template/defaultfilters.py".
    """
    title = unicodedata.normalize("NFKD", title).encode("ascii", "ignore")
    title = _slugify_strip_re.sub("", title.decode("ascii")).strip().lower()
    return _slugify_hyphenate_re.sub("-", title)[: (80 - 33)] + f"-{post_id}"
