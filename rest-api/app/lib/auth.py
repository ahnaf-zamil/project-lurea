import jwt

from functools import wraps
from flask import abort, request, g
from ..models import User
from ..config import AppConfig
from typing import Optional


def auth_required(f):
    """Auth checking decorator"""

    @wraps(f)
    def inner(*args, **kwargs):
        auth_jwt = str(request.cookies.get("auth"))
        try:
            decoded_jwt = jwt.decode(auth_jwt, AppConfig.JWT_KEY, algorithms=["HS256"])
            g.user_id = decoded_jwt["id"]
        except jwt.PyJWTError:
            abort(401)

        return f(*args, **kwargs)

    return inner


def get_authed_user() -> Optional[User]:
    """Gets the currently authenticated user based on request context"""
    user_id = g.get("user_id")
    if not user_id:
        return None

    existing_user: Optional[User] = User.query.filter(
        User.id == user_id, User.deleted == False
    ).first()
    return existing_user
