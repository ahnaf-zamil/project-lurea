import os


class AppConfig:
    SQLALCHEMY_DATABASE_URI = os.environ["DB_URI"]
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = False

    SECRET_KEY = os.environ["SECRET_KEY"]
    JWT_KEY = os.environ["JWT_KEY"]
    DEV = True if os.environ["DEV"].lower() == "true" else False
    API_HOST = os.environ["API_HOST"]

    MINIO_BUCKET = os.environ["MINIO_BUCKET"]
    MINIO_ENDPOINT = os.environ["MINIO_HOST"]
    MINIO_ACCESS_KEY = os.environ["MINIO_ACCESS_KEY"]
    MINIO_SECRET_KEY = os.environ["MINIO_SECRET_KEY"]
