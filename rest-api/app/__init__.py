from flask import Flask, jsonify
from werkzeug.exceptions import HTTPException
from .lib.ext import db, migrate, cors, bcrypt
from .config import AppConfig
from flask_swagger import swagger
from flask_swagger_ui import get_swaggerui_blueprint
from ._about import __VERSION__, __PROJECT_NAME__

# Importing models to be detected by alembic
from .models import *  # noqa: F401, F403
from .errors import handle_http_err


def make_app():
    app = Flask(__name__)
    app.config.from_object(AppConfig)

    bcrypt.init_app(app)
    cors.init_app(app)
    db.init_app(app)
    migrate.init_app(app=app, db=db)

    from .routes import all_routes

    app.register_blueprint(all_routes)
    app.register_error_handler(HTTPException, handle_http_err)

    # Swagger setup
    @app.route("/spec")
    def spec():
        swag = swagger(app)
        swag["info"]["version"] = __VERSION__
        swag["info"]["title"] = __PROJECT_NAME__
        return jsonify(swag)

    swaggerui_blueprint = get_swaggerui_blueprint(
        "/docs",
        (AppConfig.API_HOST if not AppConfig.DEV else "http://localhost:5000")
        + "/spec",  # noqa: W503
        config={"app_name": __PROJECT_NAME__},  # Swagger UI config overrides
    )
    app.register_blueprint(swaggerui_blueprint)

    return app
