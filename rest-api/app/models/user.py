from uuid import uuid4
from datetime import datetime
from ..lib.ext import db


class User(db.Model):
    __tablename__ = "users"

    id: str = db.Column(db.String(32), primary_key=True, default=lambda: uuid4().hex)
    username: str = db.Column(db.String(30), nullable=False, unique=True)
    fullname: str = db.Column(db.String(30), nullable=False)
    email: str = db.Column(db.String(255), unique=True, nullable=False)
    password: str = db.Column(db.Text, nullable=False)
    created_at: datetime = db.Column(
        db.DateTime, nullable=False, default=datetime.utcnow
    )
    deleted: bool = db.Column(db.Boolean, nullable=False, default=False)

    posts = db.relationship("Post", backref="posts")

    def json(self, preview=True):
        data = {
            "id": self.id,
            "fullname": self.fullname,
            "username": self.username,
            "created_at": str(int(self.created_at.timestamp())),
        }

        if not preview:
            data["email"] = self.email

        return data
