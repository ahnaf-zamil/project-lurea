from uuid import uuid4
from datetime import datetime
from ..lib.ext import db
from typing import List


post_tag_map = db.Table(
    "post_tag_map",
    db.Model.metadata,
    db.Column("post_id", db.String(32), db.ForeignKey("posts.id"), primary_key=True),
    db.Column("tag_id", db.String(32), db.ForeignKey("tags.id"), primary_key=True),
)


class Post(db.Model):
    __tablename__ = "posts"

    id: str = db.Column(db.String(32), primary_key=True, default=lambda: uuid4().hex)
    title: str = db.Column(db.String(50), nullable=False)
    description: str = db.Column(db.Text, nullable=False)
    created_at: datetime = db.Column(
        db.DateTime, nullable=False, default=datetime.utcnow
    )
    deleted: bool = db.Column(db.Boolean, nullable=False, default=False)

    slug: str = db.Column(db.String(80), nullable=False, unique=True)
    author_id: str = db.Column(db.String(32), db.ForeignKey("users.id"), nullable=False)
    images: List["PostImage"] = db.relationship("PostImage", backref="post_img")
    tags: List["Tag"] = db.relationship(
        "Tag", secondary=post_tag_map, back_populates="posts"
    )

    def json(self, preview=True):
        data = {
            "id": self.id,
            "title": self.title,
            "created_at": str(int(self.created_at.timestamp())),
            "tags": [i.tag_name for i in self.tags],
        }

        if not preview:
            data.update({"description": self.description, "author": self.author_id})

        return data


class PostImage(db.Model):
    __tablename__ = "post_img"

    id: int = db.Column(db.Integer, primary_key=True)

    filename: str = db.Column(db.String(10), nullable=False)
    post_id: str = db.Column(db.String(32), db.ForeignKey("posts.id"), nullable=False)


class Tag(db.Model):
    __tablename__ = "tags"

    id: int = db.Column(db.String(32), primary_key=True)

    value: str = db.Column(db.String(10), nullable=False, unique=True)
    created_at: datetime = db.Column(
        db.DateTime, nullable=False, default=datetime.utcnow
    )
    posts = db.relationship("Post", secondary=post_tag_map, back_populates="tags")

    def json(self):
        return {"id": self.id, "value": self.value, "created_at": self.created_at}
