# Alpino

A platform for people to express their creativity. Your imagination is the only limit. If you can fit your creation into pictures, you can post them here.

Wrote a story? Scan and publish. Made a robot? Take a picture and post. Drew an anime girl? HECK YEAH, post it (and also send it to me)!!

## Codebase

1. REST API -> [`Link`](./rest-api/)
1. Web Client -> [`Link`](./web-client/)

## Contact

You can email me at `hi[at]ahnafzamil[dot]com`, or message me on my Discord `Ahnaf#4346`.

## License

Specified in the [LICENSE](./LICENSE) file of the repository. Author holds all rights to the code and can impose conditions upon will.
